- Install or upgrade PeerTube

  - [Any OS (recommended)](install-any-os.md)
  - [Docker](install-docker.md)
  - [Unofficial](install-unofficial.md)


- Maintain PeerTube (sysadmin)

 - [CLI tools](maintain-tools.md)
 - [Remote storage (S3)](admin-remote-storage.md)
 - [Configurations (security, cache...)](maintain-configuration.md)
 - [Instance migration](maintain-migration.md)


- Administer PeerTube

 - [Instance follows & redundancy](admin-following-instances.md)
 - [Managing users](admin-managing-users.md)
 - [Moderate your instance](admin-moderation.md)
 - [Customize your instance](admin-customize-instance.md)
 - [PeerTube logs](admin-logs.md)


- Use PeerTube

 - [Watch, share, download a video](use-watch-video.md)
 - [Search](use-search.md)
 - [Setup your account](use-setup-account.md)
 - [User library](use-library.md)
 - [Mute instances/accounts](use-mute.md)
 - [Third-party applications](use-third-party-application.md)

- Contribute on PeerTube

 - [Getting started](contribute-getting-started.md)
 - [Plugins & Themes](contribute-plugins.md)
 - [Architecture](contribute-architecture.md)
 - [Code of conduct](contribute-code-of-conduct.md)

- PeerTube API

 - [Getting started with REST API](api-rest-getting-started.md)
 - [REST API reference](api-rest-reference.html ':ignore')
 - [Plugins & Themes API](api-plugins.md)
 - [ActivityPub](api-activitypub.md)
